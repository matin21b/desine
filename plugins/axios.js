import axios from 'axios';

const base_url = 'https://api.aramin.ir/api/v1/';
const methods = {
    post: axios.post,
    get: axios.get,
    request: axios.request,
    delete: axios.delete,
    head: axios.head,
    options: axios.options,
    put: axios.put,
    patch: axios.patch
};
const params = {
    filters: {},
    orderBy: "DESC",
    page: 1,
    paginate: true,
    paginate_num: 1,
    sortBy: "created_at"
};

export default ({ $axios }, inject) => {
    inject('dataApi', (url, type, data = {}, input_params = {}) => {
        const axiosMethod = methods[type];

        if (type === 'post' && data.hasOwnProperty("headers")) {
            axios.defaults.headers.common.Authorization = data.headers.Authorization;
        }

        let api_param = {};
        if (Object.keys(input_params).length != 0) {
            api_param = input_params;
        } else {
            api_param = params;
        }
        data = { ...api_param, ...data };

        return new Promise((resolve, reject) => {
            axiosMethod(`${base_url}${url}`, data)
                .then(response => {
                    if (response.status === 200)
                        resolve(response);
                    else
                        reject('rejected by this error');
                })
                .catch(error => {
                    reject(error.message);
                });
        });
    });
};

